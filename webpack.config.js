const path = require('path');

module.exports = {
    entry: './src/Conway.tsx',
    mode: 'development',
    devtool: 'inline-source-map',
    devServer: {
        port: 8000,
        hot: true
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/
            },
            {
                test: /\.css$/i,
                use: ['style-loader', 'css-loader']
            }
        ],

    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js'],
    },
    output: {
        filename: 'Conway.js',
        path: path.resolve(__dirname, 'dist'),
        publicPath: '/build'
    },
    cache: false
};