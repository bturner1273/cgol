'use-strict';
import React, {CSSProperties} from 'react';
import ReactDOM from 'react-dom';
import "./Conway.css";
import {ConwayBox} from './ConwayBox';

export interface IConwayGOLProps {
    numRows: number;
    numColumns: number;
    conwayBoxHoverColor?: string;
    conwayBoxAliveColor?: string;
    conwayBoxDeadColor?: string;
    conwayGOLRefreshPeriodMs?: number;
}

export interface IConwayGOLState {
    numRows: number;
    numColumns: number;
    grid: Array<Array<boolean>>;
    isEditMode: boolean;
    vpWidth: number;
    conwayBoxHoverColor: string;
    conwayBoxAliveColor: string;
    conwayBoxDeadColor: string;
}

export class ConwayGOL extends React.Component<IConwayGOLProps, IConwayGOLState> {
    private golInterval;
    constructor(props: IConwayGOLProps) {
        super(props);
        this.state = {
            numRows: props.numRows,
            numColumns: props.numColumns,
            grid: this.initGridHelper(),
            isEditMode: true,
            vpWidth: this.calculateViewportWidthHelper(),
            conwayBoxHoverColor: this.props.conwayBoxHoverColor ?? 'gray',
            conwayBoxAliveColor: this.props.conwayBoxAliveColor ?? 'white',
            conwayBoxDeadColor: this.props.conwayBoxDeadColor ?? 'black'
        }
    }

    private initGridHelper = (): Array<Array<boolean>> => 
        Array.from(
            {
                length: this.props.numRows
            }, 
            _ => new Array(this.props.numColumns).fill(false)
        ) as Array<Array<boolean>>;
    private initGrid = (): void => this.setState({grid: this.initGridHelper()});
    private getGridShallowCopy = (): Array<Array<boolean>> => {
        let gridShallowCopy: Array<Array<boolean>> = [];
        this.state.grid.map(r => gridShallowCopy.push(r.slice()));
        return gridShallowCopy;
    }

    private calculateViewportWidthHelper = () => Math.max(document.documentElement.clientWidth || 0, window.innerWidth || 0) - 30;
    private calculateViewportWidth = (): void => this.setState({vpWidth: this.calculateViewportWidthHelper()});

    componentDidMount() { 
        window.onresize = this.calculateViewportWidth;
        document.body.onkeyup = ({code}: KeyboardEvent) => {
            if (code === 'Enter' && this.state.isEditMode) {
                this.setState({isEditMode: false});
                //clear gray boxes
                const divs = document.getElementsByTagName('div');
                for (let i = 0; i < divs.length; i++) {
                    if (divs[i].style.backgroundColor === this.state.conwayBoxHoverColor) { 
                        divs[i].style.backgroundColor = this.state.conwayBoxDeadColor;
                    }
                }
                this.golInterval = setInterval(() => {
                    let g = this.getGridShallowCopy();
                    for (let row = 0; row < g.length; row++) {
                        for (let column = 0; column < g[row].length; column++) {
                            let neighbors: Array<boolean> = [
                                this.state.grid[row-1] ? this.state.grid[row-1][column-1] : false,
                                this.state.grid[row-1] ? this.state.grid[row-1][column] : false,
                                this.state.grid[row-1] ? this.state.grid[row-1][column+1] : false, 
                                this.state.grid[row][column-1],
                                this.state.grid[row][column+1],
                                this.state.grid[row+1] ? this.state.grid[row+1][column-1] : false,
                                this.state.grid[row+1] ? this.state.grid[row+1][column] : false,
                                this.state.grid[row+1] ? this.state.grid[row+1][column+1] : false
                            ];
                            let liveNeighborCount = neighbors.filter(n => n === true).length;
                            switch (liveNeighborCount) {
                                case 0:
                                case 1:
                                    g[row][column] = false;
                                    break;
                                case 2:
                                    break;
                                case 3:
                                    g[row][column] = true;
                                    break;
                                default:
                                    g[row][column] = false;
                                    break;
                            }
                        }
                    }
                    this.setState({grid: g});
                }, this.props.conwayGOLRefreshPeriodMs ?? 1000);
            }
            else if (code == "Enter" && !this.state.isEditMode) {
                //start over
                clearInterval(this.golInterval);
                this.setState({isEditMode: true});
                this.initGrid();
            }
        }
    }

    conwayBox_onMouseEnter: React.MouseEventHandler<HTMLDivElement> = ({target}: React.MouseEvent<HTMLDivElement>): void  => {
        (target as HTMLDivElement).style.backgroundColor = this.state.conwayBoxHoverColor;
    }
    conwayBox_onMouseLeave: React.MouseEventHandler<HTMLDivElement> = ({target}: React.MouseEvent<HTMLDivElement>): void => {
        var box = (target as HTMLDivElement);
        box.style.backgroundColor = box.dataset.isAlive === 'true' ? 
            (this.state.conwayBoxAliveColor) : 
            (this.state.conwayBoxDeadColor);
    }
    conwayBox_onClick = (row: number, column: number): void => {
        if (!this.state.isEditMode) return;
        let gridShallowCopy = this.getGridShallowCopy();
        gridShallowCopy[row][column] = !gridShallowCopy[row][column];
        this.setState({grid: gridShallowCopy});
    }

    render() {
        let [conwayBoxMouseEnter, conwayBoxMouseLeave, conwayBoxClick] = this.state.isEditMode ? 
            [this.conwayBox_onMouseEnter, this.conwayBox_onMouseLeave, this.conwayBox_onClick] : 
            Array(3).fill(undefined); 
        return (
            <div>
                <div>
                    {[...new Array(this.props.numRows)].map((_, i) =>
                        <div key={`i${i}`} style={{margin: 0, padding: 0, height: this.state.vpWidth / this.state.numColumns}}>
                            {[...new Array(this.props.numColumns)].map((_, j) => 
                                <ConwayBox 
                                isAlive={this.state.grid[i][j]} 
                                height={this.state.vpWidth / this.state.numColumns} 
                                width={this.state.vpWidth / this.state.numColumns} 
                                key={`i${i}j${j}`}
                                aliveColor={this.state.conwayBoxAliveColor}
                                deadColor={this.state.conwayBoxDeadColor} 
                                onMouseEnter={conwayBoxMouseEnter} 
                                onMouseLeave={conwayBoxMouseLeave} 
                                onClick={_ => conwayBoxClick(i, j)}></ConwayBox>    
                            )}
                        </div>
                    )}
                </div>
            </div>
        );
    }
}


export const renderConwayGOL = (parentId: string) => ReactDOM.render(<ConwayGOL numRows={50} numColumns={100} conwayBoxHoverColor="pink" conwayBoxAliveColor="teal" conwayBoxDeadColor="black" conwayGOLRefreshPeriodMs={100}></ConwayGOL>, document.getElementById(parentId));
window["ConwayGOL"] = {
    render: renderConwayGOL
};