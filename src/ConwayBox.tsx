import React from 'react';
import "./Conway.css";

export interface IConwayBoxProps {
    isAlive: boolean;
    aliveColor?: string;
    deadColor?: string;
    height: number | string;
    width: number | string;
    onMouseEnter: React.MouseEventHandler<HTMLDivElement>,
    onMouseLeave: React.MouseEventHandler<HTMLDivElement>,
    onClick: React.MouseEventHandler<HTMLDivElement>
}

export const ConwayBox: React.FunctionComponent<IConwayBoxProps> = (props: IConwayBoxProps) => 
<div 
    style={{
        height: props.height,
width: props.width,
        backgroundColor: props.isAlive ? (props.aliveColor ?? 'white') : (props.deadColor ?? 'black'),
    }} 
    className="conway-box"
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
    onClick={props.onClick}
    data-is-alive={props.isAlive}
></div>